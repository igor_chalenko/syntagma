if (_cpm_initialized)
    _calm_set_cpm_arguments(fmt GITHUB_REPOSITORY fmtlib/fmt GIT_TAG ${_git_tag})
endif()
_calm_find_package(fmt REQUIRED)
