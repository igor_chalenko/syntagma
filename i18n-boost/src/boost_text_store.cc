// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Sipapu - C++ logical programming library
/// @details todo add some details

#include "syntagma/i18n-boost/boost_text_store.h"

namespace syntagma::i18n {

void boost_text_store::set_locale(const std::string &posix_name) {
    // Generate locales and imbue them to the output streams
    std::locale::global(locale_generator_(posix_name));
    std::cout.imbue(std::locale());
    std::cerr.imbue(std::locale());
}

void boost_text_store::add_search_path(const std::string &path) {
    locale_generator_.add_messages_path(path);
}

std::string boost_text_store::translate(const std::string &text) {
    auto translation = ::boost::locale::translate(domain_, text);
    return translation;
}

boost_text_store::boost_text_store(
        const std::string &domain, const std::string &path) : domain_(domain) {
    locale_generator_.add_messages_domain(domain);
    add_search_path(path);
}

}