// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Sipapu - C++ logical programming library
/// @details todo add some details

#include "syntagma/i18n-boost/boost_text_store.h"
#include <gtest/gtest.h>

using syntagma::i18n::boost_text_store;

TEST(localized_text_store, set_locale) { // NOLINT
    boost_text_store store("test", "mo");

    store.set_locale("ru_RU.utf8");

    std::cout << store.format("The message is: {1}", "x") << std::endl;
    store.set_locale("es_ES.utf8");
    std::cout << store.format("The message is: {1}", "y") << std::endl;
}