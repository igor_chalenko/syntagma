// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Sipapu - C++ logical programming library
/// @details todo add some details

#ifndef SYNTAGMA_BOOST_INCLUDE_SYNTAGMA_BOOST_BOOST_LOCALIZED_TEXT_STORE_H_
#define SYNTAGMA_BOOST_INCLUDE_SYNTAGMA_BOOST_BOOST_LOCALIZED_TEXT_STORE_H_

#include <boost/locale.hpp>

#include "syntagma/i18n/text_store.h"

namespace syntagma::i18n {

// char8_t ?
//using boost_localized_message = ::boost::locale::basic_message<char>;

class boost_text_store : public text_store {
    std::string domain_;
    ::boost::locale::generator locale_generator_;

  public:
    boost_text_store(const std::string &domain, const std::string &path);

    void add_search_path(const std::string &path);

    void set_locale(const std::string &posix_name) override;

    std::string translate(const std::string &text) override;

    template<typename ...Ts>
    std::string format(const std::string &message, Ts &&... parameters) {
        std::stringstream str;
        str << (::boost::locale::format(translate(message)) % ...  % std::forward<Ts>(parameters));
        return str.str();
    }
};

}

#endif //SYNTAGMA_BOOST_INCLUDE_SYNTAGMA_BOOST_BOOST_LOCALIZED_TEXT_STORE_H_
