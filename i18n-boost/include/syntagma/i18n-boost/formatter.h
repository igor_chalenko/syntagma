//
// Created by crusoe on 16.10.2020.
//

#ifndef SYNTAGMA_FORMAT_H
#define SYNTAGMA_FORMAT_H

#include <fmt/format.h>

namespace syntagma::i18n {

template<typename... Ts>
std::string format(const std::string &message, Ts &&... arguments) {
    return fmt::format(message, std::forward<Ts>(arguments)...);
}

}// namespace syntagma::i18n

#endif//SYNTAGMA_FORMAT_H
