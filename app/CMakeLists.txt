project(syntagma-app VERSION ${PROJECT_VERSION} LANGUAGES CXX)

calm_dependency_management(
        Boost::di:v1.2.0
)

calm_add_library(${PROJECT_NAME} INTERFACE
        TEST_SOURCES tests
        DEPENDENCIES syntagma-i18n-boost Boost::di)

