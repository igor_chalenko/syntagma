#ifndef SYNTAGMA_APP_H_
#define SYNTAGMA_APP_H_

#include <memory>
#include "syntagma/i18n/text_store.h"
#include "syntagma/i18n-boost/formatter.h"

namespace syntagma::app {

struct application {
  private:
    i18n::text_store &text_store_;

  public:
    explicit application(i18n::text_store &ptr) : text_store_(ptr) {}

    template <typename... Ts>
    std::string format(const std::string &message, Ts &&... arguments) {
        auto string_template = std::move(text_store_.translate(message));
        return syntagma::i18n::format(string_template, std::forward<Ts>(arguments)...);
    }

    virtual void run() = 0;
};

}

#endif //SYNTAGMA_APP_H_
