#include <gtest/gtest.h>
#include <boost/di.hpp>

#include "syntagma/app.h"
#include "syntagma/i18n-boost/boost_text_store.h"

namespace di = boost::di;
namespace i18n = syntagma::i18n;

struct test_application : public syntagma::app::application {
    explicit test_application(i18n::text_store &ptr) : syntagma::app::application(ptr) {}

    void run() override {
        using namespace fmt::literals;
        print("arg #1: {arg1}, arg #2: {arg2}", "arg1"_a = 42, "arg2"_a=3.1415);
    }

    template<typename ...Ts>
    void print(const std::string &message, Ts &&... args) {
        std::cout << format(message, std::forward<Ts>(args)...);
    }
};

TEST(app, injection) { // NOLINT
  const auto injector = di::make_injector(
      di::bind<i18n::text_store>.to<i18n::boost_text_store>()
  );

  auto app = injector.create<test_application>();
  app.run();
}