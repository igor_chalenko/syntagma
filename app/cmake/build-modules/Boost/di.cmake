if (_cpm_initialized)
    _calm_set_cpm_arguments(boost_ext_di
            GITHUB_REPOSITORY boost-ext/di
            GIT_TAG ${_git_tag})
endif()
_calm_find_package(boost_ext_di ${_git_tag})

if (NOT TARGET Boost::di)
    #add_library(boost_di_imported INTERFACE IMPORTED GLOBAL)
    add_library(Boost::di ALIAS Boost.DI)
    #target_include_directories(boost_di_imported INTERFACE ${boost_di_INCLUDE_DIR})
    #target_link_libraries(boost_di_imported INTERFACE Boost.DI)
endif()
