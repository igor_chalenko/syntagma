include(${CMAKE_SOURCE_DIR}/cmake/get-cpm.cmake)
get_property(_cpm_initialized GLOBAL PROPERTY CPM_INITIALIZED)
if (_cpm_initialized)
    CPMFindPackage(NAME calm-cmake
            GIT_TAG master
            GITHUB_REPOSITORY igor-chalenko/calm-cmake
            GIT_TAG main
            FIND_PACKAGE_ARGUMENTS REQUIRED)
else()
    find_package(calm-cmake REQUIRED)
endif()